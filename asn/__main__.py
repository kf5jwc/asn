#!/usr/bin/env python3.8

############################################################################################################
# ----------------------------------------------------------------------
# ASN/IPv4/Prefix lookup tool. Uses Team Cymru's whois service for data.
# ----------------------------------------------------------------------
# example usage:
#  asn <ASnumber>      -- to lookup matching ASN data. Supports "as123" and "123" formats (case insensitive)
#  asn <IP.AD.DR.ESS>  -- to lookup matching route and ASN data
#  asn <ROUTE>         -- to lookup matching ASN data
#  asn <host.name.tld> -- to lookup matching IP, route and ASN data (supports multiple IPs - e.g. DNS RR)
#
# Author: Kyle Jones - 2019
# Author: Adriano Provvisiero - BV Networks 2017
#
# https://gist.github.com/nitefood/1eba4183012dcca0f082535f0eb128db
#
############################################################################################################

import argparse
import socket
import subprocess
import re
import itertools
from collections import Iterable
from functools import singledispatch
from concurrent.futures import ThreadPoolExecutor
from typing import Union, Optional, List
from ipaddress import (
    ip_address,
    ip_network,
    IPv4Address,
    IPv6Address,
    IPv4Network,
    IPv6Network,
)
import colorama
from colorama import Fore, Style
from dns import resolver


IP = Union[IPv4Address, IPv6Address, IPv4Network, IPv6Network]
IPADDR = Union[IPv4Address, IPv6Address]
IPNETWORK = Union[IPv4Network, IPv6Network]


class ASN(int):
    def __new__(cls, asn: str):
        asn_match = re.match(r"^([aA][sS])?(\d+)$", asn)
        if not asn_match:
            raise ValueError()
        return int.__new__(ASN, asn_match.group(2))


class Route:
    ip: IPADDR
    network: IPNETWORK

    def __init__(self, ip: IPADDR, network: IPNETWORK):
        self.ip = ip
        self.network = network


class RouteInfo:
    asn: ASN
    asname: str
    route: Optional[Route]

    def __init__(self, asn: ASN, asname: str, route: Optional[Route] = None):
        self.asn = asn
        self.asname = asname
        self.route = route

    def __str__(self) -> str:
        if self.route:
            return f"{Fore.WHITE}{self.route.ip!s: >15} {Fore.WHITE}-> {Fore.YELLOW}{self.route.network!s: >18} {Fore.WHITE}-> {Fore.RED}[AS{self.asn!s}] {Fore.GREEN}{self.asname}{Style.RESET_ALL}"
        else:
            return (
                f"{Fore.RED}[AS{self.asn}] {Fore.GREEN}{self.asname}{Style.RESET_ALL}"
            )


def lookup(input_str: str) -> Union[ASN, List[IPADDR]]:
    try:
        network = ip_network(input_str, strict=False)
        ip = next(iter(network))
        return [
            ip,
        ]
    except ValueError:
        pass

    try:
        asn = ASN(input_str)
        return asn
    except ValueError:
        pass

    try:
        ip = ip_address(input_str)
        return [
            ip,
        ]
    except ValueError:
        pass

    try:
        A = resolver.query(input_str, "A")
    except resolver.NoAnswer:
        A = []

    try:
        AAAA = resolver.query(input_str, "AAAA")
    except resolver.NoAnswer:
        AAAA = []

    if not any([A, AAAA]):
        raise ValueError()

    return [ip_address(rdata.address) for rdata in itertools.chain(A, AAAA)]


@singledispatch
def whois(query) -> RouteInfo:
    return NotImplemented


@whois.register(ASN)
def whois_asn(query: ASN) -> RouteInfo:
    output = subprocess.check_output(
        ["whois", "-h", "whois.cymru.com", f" -f -w -c -p as{query!s}"]
    ).decode()
    CYMRU_RESPONSE = re.compile(r"(\d+)[^\|]*\|\s+\S+[^\|]*\|\s+(.*)")
    response = CYMRU_RESPONSE.match(output)
    if not response:
        raise ValueError(output)
    (asn, asname) = response.groups()
    return RouteInfo(ASN(asn), asname)


@whois.register(IPv4Address)
@whois.register(IPv6Address)
def whois_ip(query: IPADDR) -> RouteInfo:
    output = subprocess.check_output(
        ["whois", "-h", "whois.cymru.com", f" -f -p {query.exploded}"]
    ).decode()
    CYMRU_RESPONSE = re.compile(r"(\d+)[^\|]*\|[^\|]*\|\s+(\S+)[^\|]*\|\s+(.*)")
    response = CYMRU_RESPONSE.match(output)
    if not response:
        raise ValueError(output)
    (asn, route_str, asname) = response.groups()
    route = Route(query, ip_network(route_str))
    return RouteInfo(ASN(asn), asname, route)


def cli():
    colorama.init()
    parser = argparse.ArgumentParser()
    parser.add_argument("query", type=lookup, nargs="+", help="An ASN, IP, network, or domain to gather information about.")
    queries = parser.parse_args().query

    with ThreadPoolExecutor() as executor:
        for result in executor.map(whois, flatten(queries)):
            print(result, flush=True)

def flatten(l):
    for el in l:
        if isinstance(el, Iterable) and not isinstance(el, (str, bytes)):
            yield from flatten(el)
        else:
            yield el

if __name__ == "__main__":
    cli()
